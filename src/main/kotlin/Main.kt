import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.html.respondHtml
import io.ktor.routing.Routing
import io.ktor.routing.get
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import kotlinx.html.body
import kotlinx.html.div
import kotlinx.html.h1

fun Application.mainModule() {
    install(Routing) {
        get("/") {
            call.respondHtml {
                body {
                    h1 { +"This is a heading" }

                    val s = "test"
                    div {
                        +s
                    }
                }
            }
        }
    }
}

fun main() {
    val port = 8080
    val host = "localhost"
    embeddedServer(Netty, port, host = host, module = Application::mainModule).start()
}